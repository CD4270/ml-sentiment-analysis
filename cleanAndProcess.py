import pandas as pd
import re
from sqlalchemy import create_engine
import pymysql
import plotly.express as px
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier

# Load from CSV and assign columns
TwitterData = pd.read_csv("trainingdata.csv", encoding='latin-1', names=["polarity", "tweetId", "date", "query", "username","tweet"])

# Drop query since It's not needed and every row has the same value
TwitterData = TwitterData.drop("query", axis=1)

ownDataSet = [
    {"polarity": 4, "tweetId": 2467810369, "date": "Mon Mar 31 22:19:45 PDT 2009","username": "Norman255", "tweet": "The weather is very nice today"},
    {"polarity": 0, "tweetId": 3467810369, "date": "Tue Apr 01 22:20:45 PDT 2009", "username": "Arnold63", "tweet": "Winter is coming.."},
    {"polarity": 4, "tweetId": 4467810369, "date": "Wed Apr 02 15:19:35 PDT 2009", "username": "Kai4_", "tweet": "Twitter is an awesome place to hang around"},
    {"polarity": 4, "tweetId": 5467810369, "date": "Thu Apr 03 22:17:45 PDT 2009", "username": "Arie_wetering", "tweet": "vey nice book"},
    {"polarity": 0, "tweetId": 6467810369, "date": "Fri Apr 04 20:19:45 PDT 2009", "username": "Timo55", "tweet": "Lorem ipsum..."},
    {"polarity": 0, "tweetId": 7467810369, "date": "Sat Apr 05 22:19:45 PDT 2009", "username": "Wes888_45", "tweet": "What time is it?"},
    {"polarity": 0, "tweetId": 8467810369, "date": "Sun Apr 06 12:19:45 PDT 2009", "username": "Bot4256fdf", "tweet": "What books are recommended for this winter?"},
    {"polarity": 0, "tweetId": 9467810369, "date": "Mon Apr 07 22:19:45 PDT 2009", "username": "original49", "tweet": "Searching for a dogname, can anyone help?"},
    {"polarity": 4, "tweetId": 9567810369, "date": "Tue Apr 08 18:19:45 PDT 2009", "username": "Jaime663", "tweet": "LOVED IT! the movie was amazing. Top 10 for sure, briliant acting!"},
    {"polarity": 0, "tweetId": 9667810369, "date": "Wed Apr 09 22:19:45 PDT 2009", "username": "Larisa56", "tweet": "I can't believe how bad this movie is, OMG it can't get worse than this. TOTAL JUNK!"}
]

# Add own dataset to the end of the current set
TwitterData = TwitterData.append(ownDataSet, ignore_index=True)

# For testing varity set the first 2K rows to positive polarity
TwitterData["polarity"][0:2000] = 4

# Here starts data cleaning 
stop_words = ["ourselves", "hers", "between", "yourself", "but", "again", "there", "about", "once", "during", "out", "very", "having", "with", "they", "own", "an", "be", "some", "for", "do", "its", "yours", "such", "into", "of", "most", "itself", "other", "off", "is", "s", "am", "or", "who", "as", "from", "him", "each", "the", "themselves", "until", "below", "are", "we", "these", "your", "his", "through", "don", "nor", "me", "were", "her", "more", "himself", "this", "down", "should", "our", "their", "while", "above", "both", "up", "to", "ours", "had", "she", "all", "no", "when", "at", "any", "before", "them", "same", "and", "been", "have", "in", "will", "on", "does", "yourselves", "then", "that", "because", "what", "over", "why", "so", "can", "did", "not", "now", "under", "he", "you", "herself", "has", "just", "where", "too", "only", "myself", "which", "those", "i", "after", "few", "whom", "t", "being", "if", "theirs", "my", "against", "a", "by", "doing", "it", "how", "further", "was", "here", "than"] 

TwitterData['tweet'] = TwitterData['tweet'].apply(lambda x: ' '.join([item for item in x.split() if item not in stop_words]))

def removeMentionsAndLinks(tweet):
    tweet = re.sub(r'@[A-Za-z0-9]+','', tweet)
    tweet = re.sub(r'http\S+', '', tweet)
    
    return tweet

pd.options.display.max_colwidth = 500

TwitterData["tweet"] = TwitterData["tweet"].apply(removeMentionsAndLinks)

TwitterData["tweet"] = TwitterData["tweet"].str.strip()
TwitterData["tweet"] = TwitterData["tweet"].str.replace(r'^-','',regex=True)

# Here we give only 10K rows just for testing purposes
vectorizer = TfidfVectorizer(use_idf=True, ngram_range=(1,1))
vectorizedTweets = vectorizer.fit_transform(TwitterData["tweet"][0:10000])

X_train, X_test, y_train, y_test = train_test_split(vectorizedTweets, TwitterData["polarity"][0:10000], test_size=0.2)

kn = KNeighborsClassifier()
kn.fit(X_train, y_train)
y_pred = kn.predict(X_test)
print("ACCURACY: ", metrics.accuracy_score(y_test, y_pred))
print("PRECISION: ", metrics.precision_score(y_test, y_pred, pos_label=4))
print("RECALL: ", metrics.recall_score(y_test, y_pred, pos_label=4))
print("F1 SCORE: ", metrics.f1_score(y_test, y_pred, pos_label=4))

# This part is for LR testing, I gave it two of my handmade tweets just for testing
vectorizerLR = TfidfVectorizer(use_idf=True, ngram_range=(1,1))
vectorizedTweetsLR = vectorizerLR.fit_transform(TwitterData["tweet"][1600008:1600010])

X_train, X_test, y_train, y_test = train_test_split(vectorizedTweetsLR, TwitterData["polarity"][1600008:1600010], test_size=1)

# When we fit, give 4 and 0 as y label params, which stand for positive and negative
lr = LogisticRegression()
lr.fit(vectorizedTweetsLR, [4,0]) 

print("Probability of positive review:", lr.predict_proba(vectorizedTweetsLR[0])[0,1])

# Conn and create table in onboarding DB
engine = create_engine('mysql://root:@localhost/onboarding')
TwitterData.to_sql(name='twitter',con=engine,if_exists='fail',index=False)

connection = engine.connect()

stmt = 'SELECT tweet FROM twitter'
results = connection.execute(stmt).fetchall()

mutatedData = [item for t in results for item in t]

# Create WordCloud and plot
wordCloudData = " ".join([tweets for tweets in mutatedData])
wordCould = WordCloud(width=500, height=300).generate(wordCloudData)

plt.imshow(wordCould, interpolation='bilinear')
plt.axis("off")
plt.show()